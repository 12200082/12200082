from collections import defaultdict


def dfs(src, p):
    parent[src] = p
    print(src, end=' ')
    for nbr in adjList[src]:
        if nbr != p:
            dfs(nbr, src)


if __name__ == '__main__':
    adjList = defaultdict(list)
    edges = [[1, 2], [1, 3], [2, 4], [2, 5]]
    for src, dest in edges:
        adjList[src].append(dest)
        adjList[dest].append(src)
    N = len(adjList)
    parent = [-1]*(N+1)

    dfs(1, -1)
