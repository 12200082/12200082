from collections import defaultdict


def backEdges(src, p):
    visited.add(src)

    for nbr in adjList[src]:
        if nbr not in visited:
            backEdges(nbr, src)
            break
        elif nbr != p:
            print(src, nbr)


if __name__ == "__main__":
    edges = [[0, 2], [0, 3], [0, 5], [2, 4], [3, 5], [4, 5]]
    adjList = defaultdict(list)
    for src, dest in edges:
        adjList[src].append(dest)
        adjList[dest].append(src)
    visited = set()
    backEdges(0, 0)
