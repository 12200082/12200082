# from collections import defaultdict


# class Solution:
#     def findItinerary(self, tickets):
#         adjList = defaultdict(list)
#         for src, dest in tickets:
#             adjList[src].append(dest)
#         # print(adjList)
#         for l in adjList.values():
#             l.sort()
#         # print(adjList)
#         result = ["JKF"]

#         def dfs(src):
#             if len(result) == len(tickets)+1:
#                 return True
#             if src not in adjList:  # no route, backtrack
#                 return False
#             # enumerate to access both key in values.
#             for i, nbr in enumerate(adjList[src]):
#                 adjList[src].pop(i)
#                 result.append(nbr)

#                 if dfs(nbr):
#                     return True
#                 # backtracking
#                 adjList[src].insert(i, nbr)
#                 result.pop()  # bydefault last element will be removed by default pop out takes -1 index
#             return False  # end of dfs function
#         dfs("JKF")
#         return result


# if __name__ == '__main__':
#     obj = Solution()
#     tickets = [["JFK", "SFO"], ["JFK", "ATL"], [
#         "SFO", "ATL"], ["ATL", "JFK"], ["ATL", "SFO"]]
#     res = obj.findItinerary(tickets)
#     print(res)

from collections import defaultdict


class Solution:

    def findItinerary(self, tickets):

        adjList = defaultdict(list)

        for src, dest in tickets:
            adjList[src].append(dest)

        # print(adjList)

        for l in adjList.values():
            l.sort()
        # print(adjList)

        result = ["JFK"]

        def dfs(src):
            if len(result) == len(tickets)+1:
                return True

            if src not in adjList:
                return False
            # recursive
            for i, nbr in enumerate(adjList[src]):
                adjList[src].pop(i)
                result.append(nbr)

                if dfs(nbr):
                    return True

                # backtracking
                adjList[scr].insert(i, nbr)
                result.pop()
            return False

        dfs("JFK")
        return result


if __name__ == '__main__':
    obj = Solution()

    tickets = [["JFK", "SFO"], ["JFK", "ATL"], [
        "SFO", "ATL"], ["ATL", "JFK"], ["ATL", "SFO"]]
    iti = obj.findItinerary(tickets)
    print(iti)
