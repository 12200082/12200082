from collections import defaultdict


class Graph:
    # constructor
    def __init__(self):
        self.adjList = defaultdict(list)

    def add_edge(self, src, dest):
        self.adjList[src].append(dest)
        self.adjList[dest].append(src)

    # # Helper function
    # def dfsHelper(self, src, visited):
    #     visited.add(src)
    #     print(src, end=' ')
    #     for node in self.adjList[src]:
    #         if node not in visited:
    #             self.dfsHelper(node, visited)
    #     return

    # def dfs(self, src):
    #     visited = set()
    #     self.dfsHelper(src, visited)

    def dfs(self, src, visited=None):
        if visited is None:
            visited = set()
        if visited is not None:
            visited.add(src)
        print(src, end=' ')
        for nbr in self.adjList[src]:
            if nbr not in visited:
                self.dfs(nbr, visited)


if __name__ == '__main__':
    g = Graph()
    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 3)
    g.add_edge(1, 4)
    g.add_edge(2, 5)
    g.add_edge(2, 6)
    # print(g.adjList)
    g.dfs(0)
