def exPower(a,b):
    value = 1
    while b:
        if b&1:
            value=value*a
        a = a*a
        b>>=1
    return value
if __name__ == '__main__':
    print(exPower(2,5))