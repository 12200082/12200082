def pascalTriangle(n):
    table = [[0]*(n+1) for i in range(n+1)]

    for i in range(n+1):
        for r in range(i+1):
            if i == r or r == 0:
                table[i][r] = 1
            else:
                table[i][r] = table[i-1][r-1] + table[i-1][r]

    return table


def noOfWays(n, m, t):
    table = pascalTriangle(n)

    k = 0
    for i in range(4, t):
        if (t-i > m | i > n):
            continue
        k = k + (table[n][i] * table[m][t-i])
    print(k)


n = int(input('n '))
m = int(input('m '))
t = int(input('t '))
noOfWays(n, m, t)
