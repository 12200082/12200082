def pascalTriangle(n):
    table = [[0]*(n+1) for i in range(n+1)]

    for i in range(n+1):
        for r in range(i+1):
            if i == r or r == 0:
                table[i][r] = 1
            else:
                table[i][r] = table[i-1][r-1] + table[i-1][r]
    return table


def countDigit(n):
    if n//10 == 0:
        return 1
    return 1 + countDigit(n // 10)


def digits(arr):
    table = pascalTriangle(arr[0])
    n = arr[0]
    k = arr[1]
    num = table[n][k]
    print(countDigit(num))


x = int(input('test cases'))
arr = []
for i in range(x):
    n = int(input('n'))
    k = int(input('k'))
    arr.append(n)
    arr.append(k)
    digits(arr)
    arr.pop()
    arr.pop()
