def ployHash(s):
    p1, m1 = 31, 10**9+7
    p2, m2 = 37, 10**9+9
    hashValue1, hashValue2 = 0, 0
    ppow1, ppow2 = 1, 1

    for i in s:
        hashValue1 = (hashValue1 + (ord(i)-ord('a')+1)*ppow1)%m1
        hashValue1 = (hashValue2 + (ord(i)-ord('a')+1)*ppow2)%m2
        ppow1 = ppow1*p1
        ppow2 = ppow2*p2
    return hashValue1, hashValue2
if __name__ == '__main__':
    s1 = "countermand"
    s2 = "furnace"
    print(ployHash(s1))
    print(ployHash(s2))