# def countSetbit(x):
#     count =0
#     while(x):
#         x = x >> 1
#         count += x & 1        
#     return count
# if __name__ == "__main__" :
#     x = 10
#     print(countSetbit(x))

def countBits(n):
    count =  0
    while n:
        if n&1:
            count +=1
        n >>= 1
    return count 

if __name__ == '__main__':
    n = 10
    print(countBits(n))