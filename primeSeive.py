from math import sqrt
N = 20
sieve = [1 for i in range(N+1)]
def buildSieve():   
    p = 2
    while p <= sqrt(N):
        for i in range(p*p, N+1, p):
            sieve[i]=0
        p += 1
    
def findSum(n):
    sum =  0
    for i in range(2,n+1):
        if sieve[i]:
            sum += i
    return sum

if __name__ == '__main__':
    buildSieve()
    tc = int(input())
    while tc:
        n = int(input())
        print(findSum(n))
        tc -= 1
