# def Rabin_Karp_Matcher(text, pattern, d, q):
#     n = len(text)
#     m = len(pattern)
#     h = pow(d,m-1)%q
#     p = 0
#     t = 0
#     result = []
#     for i in range(m): # preprocessing
#         p = (d*p+ord(pattern[i]))%q
#         t = (d*t+ord(text[i]))%q
#     for s in range(n-m+1): # note the +1
#         if p == t: # check character by character
#             match = True
#             for i in range(m):
#                 if pattern[i] != text[s+i]:
#                     match = False
#                     break
#             if match:
#                 result = result + [s]
#         if s < n-m:
#             t = (t-h*ord(text[s]))%q # remove letter s
#             t = (t*d+ord(text[s+m]))%q # add letter s+m
#             t = (t+q)%q # make sure that t >= 0
#     return result
# print (Rabin_Karp_Matcher ("3141592653589793", "26", 257, 11))
# print (Rabin_Karp_Matcher ("xxxxx", "xx", 40999999, 999999937))


# [6]
# [0, 1, 2, 3]

p = 31
mod = 10**9+7
def power(a, n):
    result = 1
    while n:
        if n%1:
            result = (result * a)%mod
        a = (a*a)%mod
        n>>=1
    return result

def polyHash(text):
    hashValue = 0
    pPow = 1
    for i in range(len(text)):
        hashValue = (hashValue+(ord(text[i])-ord('a')+1) * pPow)%mod
        pPow = pPow*p

    return hashValue

def rabinKarp(text, pattern):
    n = len(text)
    m = len(pattern)
    pHash = polyHash(pattern) #pattern hash 
    subHash = polyHash(text[:m]) # 0 index to m...(acd)
    if pHash == subHash:
        print(0)
    pModInv = power(p, mod-2)
    for i in range(1,n-m+1):
        #Remove fisrt character
        subHash = (subHash-(ord(text[i-1])-ord('a')+1)+mod)%mod
        #Reduce a power
        subHash = (subHash*pModInv)%mod
        subHash = (subHash + (ord(text[i+m-1])-ord('a')+1)*power(p, m-1))%mod

        if subHash == pHash:
            print(i)

if __name__ == '__main__':
    text = "acdefcde"
    pattern = "cde"
    rabinKarp(text, pattern)
